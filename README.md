
# Water Reminder App Backend

This is the backend for the Water Reminder App built with FastAPI.

## Setup

1. Install dependencies:

```bash
pip install -r requirements.txt
```

2. Run the application:

```bash
uvicorn app.main:app --reload
```

3. Run the tests:

```bash
pytest
```
