from sqlalchemy.orm import Session
from . import models, schemas
from datetime import date  # Adicione esta linha

def get_user(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.id == user_id).first()

def get_users(db: Session, skip: int = 0, limit: int = 10):
    return db.query(models.User).offset(skip).limit(limit).all()

def create_user(db: Session, user: schemas.UserCreate):
    daily_goal = user.weight * 35
    db_user = models.User(name=user.name, weight=user.weight, daily_goal=daily_goal)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

def get_water_intake(db: Session, user_id: int, date: date):
    return db.query(models.WaterIntake).filter(models.WaterIntake.user_id == user_id, models.WaterIntake.date == date).first()

def create_water_intake(db: Session, water_intake: schemas.WaterIntakeCreate, user_id: int):
    db_water_intake = models.WaterIntake(**water_intake.dict(), user_id=user_id)
    db.add(db_water_intake)
    db.commit()
    db.refresh(db_water_intake)
    return db_water_intake
