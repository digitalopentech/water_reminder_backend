from sqlalchemy import Column, Integer, String, Float, Date, ForeignKey
from sqlalchemy.orm import relationship
from .database import Base

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    weight = Column(Float)
    daily_goal = Column(Float)

    water_intakes = relationship("WaterIntake", back_populates="user")

class WaterIntake(Base):
    __tablename__ = 'water_intakes'
    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    date = Column(Date)
    amount = Column(Float)

    user = relationship("User", back_populates="water_intakes")
