from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from datetime import date
from typing import List
from .. import crud, models, schemas
from ..dependencies import get_db

router = APIRouter()

@router.post("/users/{user_id}/water_intakes/", response_model=schemas.WaterIntake)
def create_water_intake(user_id: int, water_intake: schemas.WaterIntakeCreate, db: Session = Depends(get_db)):
    return crud.create_water_intake(db=db, water_intake=water_intake, user_id=user_id)

@router.get("/users/{user_id}/water_intakes/{date}", response_model=schemas.WaterIntake)
def read_water_intake(user_id: int, date: date, db: Session = Depends(get_db)):
    db_water_intake = crud.get_water_intake(db, user_id=user_id, date=date)
    if db_water_intake is None:
        raise HTTPException(status_code=404, detail="Water intake not found")
    return db_water_intake
