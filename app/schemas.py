from pydantic import BaseModel
from datetime import date
from typing import List

class UserBase(BaseModel):
    name: str
    weight: float

class UserCreate(UserBase):
    pass

class User(UserBase):
    id: int
    daily_goal: float
    water_intakes: List['WaterIntake'] = []

    class Config:
        from_attributes = True

class WaterIntakeBase(BaseModel):
    date: date
    amount: float

class WaterIntakeCreate(WaterIntakeBase):
    pass

class WaterIntake(WaterIntakeBase):
    id: int
    user_id: int

    class Config:
        from_attributes = True
