
from fastapi.testclient import TestClient
from app.main import app

client = TestClient(app)

def test_create_user():
    response = client.post("/api/v1/users/", json={"name": "John", "weight": 70.0})
    assert response.status_code == 200
    assert response.json()["name"] == "John"

def test_create_water_intake():
    response = client.post("/api/v1/users/1/water_intakes/", json={"date": "2024-06-01", "amount": 500})
    assert response.status_code == 200
    assert response.json()["amount"] == 500
